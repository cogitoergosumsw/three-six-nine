package main

import (
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	"log"
)

func getDigitalSum(input string) (int, error) {
	total := 0

	for _, digit := range input {
		value, err := strconv.Atoi(string(digit))
		if err != nil {
			return -1, fmt.Errorf("error converting input string to individual digits %s", err)
		}
		total += value
	}

	return total, nil
}

func isThreeSixNine(input string) (bool, error) {
	if _, err := strconv.ParseInt(input, 10, 64); err == nil {
		// fmt.Printf("%q looks like a number.\n", input)
	} else {
		fmt.Printf("%s is not a number\n", input)
		return false, fmt.Errorf("input is not a valid number: %s", err)
	}

	digitalRoot, err := getDigitalSum(input)
	if err != nil {
		return false, fmt.Errorf("cannot compute digital sum of %s: %s", input, err)
	}

	for digitalRoot > 9 {
		intermediate := strconv.Itoa(digitalRoot)
		digitalRoot, err = getDigitalSum(intermediate)
		if err != nil {
			return false, fmt.Errorf("cannot compute digital sum of %s: %s", input, err)
		}
	}

	if digitalRoot == 3 || digitalRoot == 6 || digitalRoot == 9 {
		return true, nil
	}

	return false, nil
}

func listAll(lastNum int) ([]int, error) {
	var allNum []int
	if lastNum < 1 {
		return nil, fmt.Errorf("input number invalid, pls give a positive number to search for three-six-nines :)")
	}
	for i := 0; i <= lastNum; i++ {
		yes, err := isThreeSixNine(strconv.Itoa(i))
		if err != nil {
			return nil, fmt.Errorf("error checking %v for three-six-nine: %s", i, err)
		}
		if yes {
			allNum = append(allNum, i)
		}
	}
	return allNum, nil
}

func main() {

	if len(os.Args) == 1 {
		err := "HELP: \ncheck if input number is a three-six-nine with --check=33 e.g. ./main --check=333\n"
		err += "list the numbers which is also three-six-nine with --list=100 e.g. ./main --list=100\n"
		fmt.Println(err)
		os.Exit(1)
	}

	var checkNum string
	var lastNum int
	flag.StringVar(&checkNum, "check", "", "check if the given input number is three-six-nine")
	flag.IntVar(&lastNum, "list", 0, "get a list of numbers that are three-six-nine")
	flag.Parse()

	if len(checkNum) > 0 {
		// input number here (in string)
		yesNo, err := isThreeSixNine(checkNum)
		if err != nil {
			log.Print(err)
		} else {
			fmt.Printf("%s is a divine num: %v \n", checkNum, yesNo)
		}
	}

	if lastNum > 0 {
		numList, err := listAll(lastNum)
		if err != nil {
			log.Print(err)
		} else {
			var outputStr []string
			for _, num := range numList {
				o := strconv.Itoa(num)
				outputStr = append(outputStr, o)
			}
			output := strings.Join(outputStr, ", ")
			fmt.Printf("list of numbers that are three-six-nines: %s \n", output)
		}
	}

	os.Exit(0)
}
