# three-six-nine

demystifying the magic of the numbers - 3, 6, 9

## Name
A project to understand better about the magic numbers - *'three'*, *'six'* and *'nine'*. 

## Description

If you think that there's something special about the numbers - 3, 6 and 9, then you're not alone. 

> “If you knew the magnificence of the three, six and nine, you would have a key to the universe.” ― Nikola Tesla

> “If you want to find the secrets of the universe, think in terms of energy, frequency and vibration.” ― Nikola Tesla

The project seeks to investigate if there's any significance of these numbers in our daily lives. Will things go better if they were in threes? 

The project is written in Go v 1.17. 

## Installation

1. Install Go on your local machine, follow the installation instructions here - https://go.dev/doc/install 
2. Build the Go binary according to your platform e.g. `linux amd64` by running the `build.sh` script e.g. `./build.sh main.go threesixnine`

## Usage

with the `--check` flag you can check if the number is a three-six-nine number i.e. `./threesixnine --check 333`
Expected output should be something like,
```bash
333 is a divine number: true
```

with the `--list` flag, you can get a list of numbers which are three-six-nine where the input number is the range limit of numbers i.e. `./threesixnine --list 100`
Expected output should be something like,
```bash
list of numbers that are three-six-nines: 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75, 78, 81, 84, 87, 90, 93, 96, 99
```

## Support
Please open issue to seek any support on the repo. We will try to fix whichever bug or feature request from there.

## Contributing

Looking for interested parties to contribute in mainly two ways,

1. Ideas on putting the 3,6,9 theory into applications
2. Coding skills to put the ideas into reality

## Authors and acknowledgment
Lead Author - cogitoergosumsw

## Reference

- https://medium.com/the-ascent/why-tesla-called-3-6-and-9-the-secret-of-the-universe-b308d8b77eb6

## License
This project is licensed under the **MIT License**.

## Project status

Status - **Active**

***

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
